# Ramanil's Cell Migration Paper

## Statistical Methods

Modelling and statistical analysis – including Ordinary Least Squares (OLS) regression, Analysis of Variance (ANOVA), t-tests, and Benjamini-Hochberg multiple comparison correction – were performed using the free and open source software package, [Statsmodels](http://conference.scipy.org/proceedings/scipy2010/pdfs/seabold.pdf).
All code used to generate the associated top-level statistics (along with the top-level data) has been made [openly accessible](https://bitbucket.org/TheChymera/rcmp/src/master/).
